export async function asyncForEach<T>(array: T[], callback: (t: T, index: number, array: T[]) => any) {
    for (let index = 0; index < array.length; index++) {
        await callback(array[index], index, array);
    }
}

export async function sleep(ms) {
    return new Promise(resolve => {
        setTimeout(resolve, ms);
    });
}

export class MemStream {
    private body = '';

    write(text: string, cb:Function) {
        this.body = this.body.concat(text);
        cb();
    }

    toString() {
        return this.body;
    }
}
