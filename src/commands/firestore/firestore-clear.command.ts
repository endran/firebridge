import {FirebaseAdmin} from '../../firebase-admin';
import {Command} from '../command';
import {ClearService} from '@endran/firestore-export-import/dist/clear.service';

export class FirestoreClearCommand implements Command {
    static KEY = 'FIRESTORE.CLEAR';
    format = `  { "action":"FIRESTORE.CLEAR", "ref": "__ROOT__" | "colA" | "colA/docB" }`;

    async execute(firebaseAdmin: FirebaseAdmin, params: any): Promise<any> {
        await new ClearService(firebaseAdmin.firestore).clear(params.ref)

        return {
            success: true
        };
    }
}
