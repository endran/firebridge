import {FirebaseAdmin} from '../../firebase-admin';
import * as fs from 'fs';
import {Command} from '../command';
import {ImportService} from '@endran/firestore-export-import/dist/import.service';

export class FirestoreImportCommand implements Command {
    static KEY = 'FIRESTORE.IMPORT';
    format = ` { "action":"FIRESTORE.IMPORT", "file": "export.json" }`;

    async execute(firebaseAdmin: FirebaseAdmin, params: any): Promise<any> {
        const file = `${process.cwd()}/${params.file}`;
        if (!fs.existsSync(file)) {
            return {
                error: `File ${file} does not exist`
            };
        }

        console.log(`Reading data from ${file}`);
        const readStream = fs.createReadStream(file);

        try {
            const importService = new ImportService(firebaseAdmin.firestore);
            await importService.import(readStream);
            console.log(`Import done`);
        } catch (e) {
            console.error('Could not export data', e);
            return {
                error: 'Could not import data'
            };
        }
        readStream.close();

        return {
            success: true
        };
    }
}
