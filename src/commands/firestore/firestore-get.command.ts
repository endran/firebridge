import {FirebaseAdmin} from '../../firebase-admin';
import {Command} from '../command';
import {ExportService} from '@endran/firestore-export-import/dist/export.service';
import {Util} from '@endran/firestore-export-import/dist/util';
import {MemStream} from '../../util';

export class FirestoreGetCommand implements Command {
    static KEY = 'FIRESTORE.GET';
    format = `    { "action":"FIRESTORE.GET", "ref": "__ROOT__" | "colA" | "colA/docB" }`;

    async execute(firebaseAdmin: FirebaseAdmin, params: any): Promise<any> {
        const memStream = new MemStream()
        try {
            const exportService = new ExportService(firebaseAdmin.firestore);
            await exportService.export(params.ref, memStream as any, Util.getTimestamp());
            console.log(`Streaming done`);
        } catch (e) {
            console.error('Could not export data', e);
            return {error: 'Could not export data'};
        }

        const data = memStream.toString();
        if (!data) {
            return {error: 'Could not export data'};
        }
        return {success: true, data};
    }
}
