import {FirebaseAdmin} from '../../firebase-admin';
import {Command} from '../command';
import {ImportService} from '@endran/firestore-export-import/dist/import.service';

const {Readable} = require('stream');

export class FirestoreSetCommand implements Command {
    static KEY = 'FIRESTORE.SET';
    format = `    { "action":"FIRESTORE.SET", "data": {"colA/docB": {}} }`;

    async execute(firebaseAdmin: FirebaseAdmin, params: any): Promise<any> {
        const readable = Readable.from(Object.keys(params.data).map(key => `"${key}":${JSON.stringify(params.data[key])},`).join("\n\r"));
        try {
            const importService = new ImportService(firebaseAdmin.firestore);
            await importService.import(readable);
            console.log(`Import done`);
        } catch (e) {
            console.error('Could not export data', e);
            return {
                error: 'Could not import data'
            };
        }

        return {
            success: true
        };
    }
}
