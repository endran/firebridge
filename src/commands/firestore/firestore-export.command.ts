import {FirebaseAdmin} from '../../firebase-admin';
import * as fs from 'fs';
import {Command} from '../command';
import {ExportService} from '@endran/firestore-export-import/dist/export.service';
import {Util} from '@endran/firestore-export-import/dist/util';

export class FirestoreExportCommand implements Command {
    static KEY = 'FIRESTORE.EXPORT';
    format = ` { "action":"FIRESTORE.EXPORT", "ref": "__ROOT__" | "colA" | "colA/docB", "file": "export.json" }`;

    async execute(firebaseAdmin: FirebaseAdmin, params: any): Promise<any> {
        console.log(`params`, params);
        const file = `${process.cwd()}/${params.file}`;
        if (fs.existsSync(file)) {
            return {error: `File ${file} already exists`};
        }

        const writeStream = fs.createWriteStream(file);

        try {
            await new ExportService(firebaseAdmin.firestore).export(params.ref, writeStream, Util.getTimestamp());
            console.log(`Streaming done`);
        } catch (e) {
            console.error('Could not export data', e);
            return {error: 'Could not export data'};
        } finally {
            writeStream.close();
        }

        return {success: true};
    }
}
