import {Command} from '../command';
import {FirebaseAdmin} from '../../firebase-admin';
import {sleep} from '../../util';

export class AuthDeleteCommand implements Command {
    static KEY = 'AUTH.DELETE';
    format = `      { "action":"AUTH.DELETE", "remove?": ".*@remove.com", "keep?": ".*@keep.com" }`;

    public async execute(firebaseAdmin: FirebaseAdmin, params: any): Promise<any> {
        const req = await firebaseAdmin.auth.listUsers();

        let keepRegex: RegExp;
        if (params.keep) {
            keepRegex = new RegExp(params.keep);
        }
        let removeRegex: RegExp;
        if (params.remove) {
            removeRegex = new RegExp(params.remove);
        }

        const users = req.users
            .filter(user => removeRegex === undefined || removeRegex.test(user.email))
            .filter(user => keepRegex === undefined || !keepRegex.test(user.email));

        while (users.length > 0) {
            let user = users.splice(0, 1)[0];
            await firebaseAdmin.auth.deleteUser(user.uid);
            if (users.length > 0) {
                await sleep(1100); // Delete quota is 10 per second
            }
        }

        return {
            success: true
        };
    }
}
