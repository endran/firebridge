import * as admin from 'firebase-admin';

export class FirebaseAdmin {
    admin = admin;
    auth: admin.auth.Auth;
    firestore: FirebaseFirestore.Firestore;

    init(path: string, emulator: number) {
        if (emulator) {
            console.log(`-- USING EMULATOR --`);
            process.env.FIRESTORE_EMULATOR_HOST = 'localhost:' + emulator;
        }
        const serviceAccount = require(`${process.cwd()}/${path}`);
        admin.initializeApp({
            credential: admin.credential.cert(serviceAccount)
        });

        this.auth = admin.auth();

        this.firestore = admin.firestore() as FirebaseFirestore.Firestore;
    }
}
